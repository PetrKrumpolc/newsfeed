﻿using System;
using System.IO;
using System.Windows;

using News.Settings;
using News.Settings.IO;

namespace News
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public string UserDataPath
        {
            get
            {
                return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}/PK/NEWSfeed";
            }
        }

        public string SettingPath
        {
            get
            {
                return $"{UserDataPath}/setting.xml";
            }
        }

        public Setting Setting
        {
            get;
            private set;
        }


        public App()
        {
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            try
            {
                CheckOrCreateUserDir();
                Setting = LoadSetting(SettingPath);
            }
            catch(Exception)
            {
                Setting = new Setting();
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            try
            {
                CheckOrCreateUserDir();
                SaveSetting(Setting, SettingPath);
            }
            catch(Exception exc)
            {
                MessageBox.Show("Error", exc.Message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void CheckOrCreateUserDir()
        {
            if (!Directory.Exists(UserDataPath))
                Directory.CreateDirectory(UserDataPath);
        }

        private Setting LoadSetting(string path)
        {
            if (File.Exists(path))
                return new SettingAccessor().Load(File.OpenRead(path));
            else
                return new Setting();
        }

        private void SaveSetting(Setting setting, string path)
        {
            new SettingAccessor().Save(File.Open(path, FileMode.Create), setting);
        }
    }
}
