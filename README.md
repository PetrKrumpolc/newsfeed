# NEWS feed #

A small .NET (C#, WPF - MVVM) desktop application for downloading news via RSS channel. It's based
on plugin architecture.

### Plugins: ###
* [Czech TV (Česká Televize)](https://www.ceskatelevize.cz/)
* [Czech Radio (Český Rozhlas)](https://portal.rozhlas.cz/)
* [CNN](https://edition.cnn.com/)

The app is easily extendable by adding new plugin that handles RSS News downloading.
(Project is no longer in the development.)