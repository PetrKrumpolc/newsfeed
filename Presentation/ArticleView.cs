﻿using System;
using System.Diagnostics;
using System.Windows.Input;

using IPlugin.Feeds;

using News.Presentation.Commands;

namespace News.Presentation
{
    public class ArticleView : ViewModelBase
    {
        #region Fields
        private readonly Article _Article;
        #endregion

        #region Properties
        public string Title
        {
            get
            {
                return _Article.Title;
            }
        }

        public string Description
        {
            get
            {
                return _Article.Description;
            }
        }

        public DateTime Published
        {
            get
            {
                return _Article.Published;
            }
        }

        public Uri ThumbnailSource
        {
            get
            {
                return _Article.ThumbnailSource;
            }
        }
        #endregion

        #region Commands
        public ICommand OpenArticleCommand
        {
            get;
        }
        #endregion


        public ArticleView(Article article)
        {
            _Article = article;

            OpenArticleCommand = new ActionCommand(OpenArticle, CanOpenArticle);
        }


        private void OpenArticle()
        {
            Process.Start(_Article.Link.AbsoluteUri);
        }

        private bool CanOpenArticle()
        {
            return _Article.Link != null && !string.IsNullOrEmpty(_Article.Link.AbsoluteUri);
        }
    }
}
