﻿using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Input;
using System.Linq;
using System.Diagnostics;

using IPlugin;

using News.Plugins;
using News.Presentation.Commands;
using News.Settings;

namespace News.Presentation
{
    public class MainView : ViewModelBase
    {
        #region Fields
        private PluginWatcher _Watcher;
        private PluginLoader  _Loader;
        private PluginView    _SelectedPlugin;
        #endregion

        #region Properties
        public ObservableCollection<PluginView> Plugins
        {
            get;
        }

        public PluginView SelectedPlugin
        {
            get
            {
                return _SelectedPlugin;
            }
            set
            {
                _SelectedPlugin = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Commands
        public ICommand LoadCommand
        {
            get;
        }

        public ICommand DestroyCommand
        {
            get;
        }
        #endregion

        public MainView()
        {
            Plugins = new ObservableCollection<PluginView>();

            LoadCommand    = new ActionCommand(Initialize);
            DestroyCommand = new ActionCommand(Destroy);

            _Watcher = new PluginWatcher();
            _Watcher.NewPlugin      += OnNewPlugin;
            _Watcher.DeletePlugin   += OnDeletePlugin;
            _Watcher.RenamedPlugin  += OnRenamedPlugin;

            _Loader = new PluginLoader();
        }


        private void Initialize()
        {
            Thread thread = new Thread(() => 
            {
                foreach (string plugin in new PluginLocator().FindPlugins())
                    Load(plugin);

                SelectedPlugin = (from plugin in Plugins 
                                  where plugin.Company == ((App)App.Current).Setting.Company
                                  select plugin).FirstOrDefault();
                if (SelectedPlugin != null)
                    SelectedPlugin.SelectChannel(((App)App.Current).Setting.ChannelUrl);
            });

            thread.Priority     = ThreadPriority.Highest;
            thread.IsBackground = true;
            thread.Start();
        }

        private void Destroy()
        {
            Debug.WriteLine("Moving Main View settings to the Settings class.");
            Setting setting = ((App)App.Current).Setting;

            setting.Company = SelectedPlugin != null ? SelectedPlugin.Company : string.Empty;
            setting.ChannelUrl = SelectedPlugin != null && SelectedPlugin.SelectedChannel != null ? 
                                 SelectedPlugin.SelectedChannel.URL.AbsoluteUri : string.Empty;
            Debug.WriteLine("Destroying Main View");

            _Watcher.Dispose();
        }



        private void OnNewPlugin(string path)
        {
            Thread thread = new Thread(() => Load(path));

            thread.Priority     = ThreadPriority.Highest;
            thread.IsBackground = true;
            thread.Start();
        }

        private void OnDeletePlugin(string path)
        {
            PluginView plugin = Plugins.First(p => p.Path.Equals(path, System.StringComparison.CurrentCultureIgnoreCase));

            if (plugin != null)
                App.Current.Dispatcher.Invoke(() => Plugins.Remove(plugin));
        }

        private void OnRenamedPlugin(string oldPath, string newPath)
        {
            PluginView plugin = Plugins.First(p => p.Path.Equals(oldPath, System.StringComparison.CurrentCultureIgnoreCase));

            if (plugin != null)
                App.Current.Dispatcher.Invoke(() => plugin.Path = newPath );
        }


        private void Load(string path)
        {
            try
            {
                IFeedPlugin feedPlugin = _Loader.Load(path);
                feedPlugin.Initialize();

                App.Current.Dispatcher.Invoke(() =>
                {
                    Plugins.Add(new PluginView(feedPlugin, path));
                    Debug.WriteLine($"Plugin loaded - {path}");
                });
            }
            catch (InvalidPluginException)
            {
                App.Current.Dispatcher.Invoke(() =>
                {
                    Plugins.Add(new PluginView(new NullFeedPlugin(), path));
                    Trace.Fail($"Plugin load hasn't been successful. Invalid plugin interface. {path}");
                });
            }
        }
    }
}
