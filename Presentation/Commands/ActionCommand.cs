﻿using System;
using System.Windows.Input;

namespace News.Presentation.Commands
{
    public class ActionCommand : ICommand
    {
        #region Fields
        readonly Action     _Execute;
        readonly Func<bool> _CanExecute;
        #endregion

        public event EventHandler CanExecuteChanged
        {
            add    { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }


        public ActionCommand(Action execute, Func<bool> canExecute = null)
        {
            _Execute    = execute;
            _CanExecute = canExecute;
        }

        public ActionCommand(Action execute)
            : this(execute, null)
        {}


        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        public bool CanExecute()
        {
            return _CanExecute == null || _CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            Execute();
        }

        public void Execute()
        {
            _Execute?.Invoke();
        }
    }
}
