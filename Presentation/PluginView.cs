﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

using IPlugin;
using IPlugin.Feeds;

using News.Presentation.Commands;

namespace News.Presentation
{
    public class PluginView : ViewModelBase
    {
        #region Fields
        private string _Path;
        private readonly IFeedPlugin _Plugin;

        private CategoryView _SelectedChannel;
        #endregion

        #region Properties
        public string Company
        {
            get
            {
                return _Plugin.Company;
            }
        }

        public string Path
        {
            get
            {
                return _Path;
            }
            set
            {
                _Path = value;
                OnPropertyChanged();
            }
        }

        public Uri LogoSource
        {
            get
            {
                return _Plugin.LogoSource;
            }
        }

        public ObservableCollection<CategoryView> Channels
        {
            get;
        }

        public CategoryView SelectedChannel
        {
            get
            {
                return _SelectedChannel;
            }
            set
            {
                _SelectedChannel = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Commands
        public ICommand RefreshCommand
        {
            get;
        }
        #endregion


        public PluginView(IFeedPlugin plugin, string path)
        {
            Path = path;
            _Plugin = plugin;

            Channels = new ObservableCollection<CategoryView>(from channel in _Plugin.Channels select new CategoryView(channel));

            RefreshCommand = new ActionCommand(Refresh, CanRefresh);
        }

        private void Update()
        {
            int selectedChannel = Channels.IndexOf(SelectedChannel);
            Channels.Clear();

            foreach (Channel channel in _Plugin.Channels)
                Channels.Add(new CategoryView(channel));

            SelectedChannel = selectedChannel != -1 ? Channels[selectedChannel] : Channels.FirstOrDefault();
        }

        public void SelectChannel(string url)
        {
            SelectedChannel = (from channel in Channels
                               where channel.URL.AbsoluteUri == url
                               select channel).FirstOrDefault();
        }

        private void Refresh()
        {
            _Plugin.Update();
            Update();
        }

        private bool CanRefresh()
        {
            return _Plugin.IsInitialized;
        }


        public void Destroy()
        {
            _Plugin.Destroy();
        }


        public override string ToString()
        {
            return Company;
        }
    }
}
