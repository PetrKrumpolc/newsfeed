﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;

using IPlugin.Feeds;
using News.Presentation.Commands;

namespace News.Presentation
{
    public class CategoryView : ViewModelBase
    {
        #region Fields
        private readonly Channel _Channel;

        private ArticleView _SelectedArticle;
        #endregion

        #region Properties
        public string Title
        {
            get
            {
                return _Channel.Title;
            }
        }

        public string Description
        {
            get
            {
                return _Channel.Description;
            }
        }

        public Uri ImageSource
        {
            get
            {
                return _Channel.ImageSource;
            }
        }

        public int ArticleCount
        {
            get
            {
                return Articles.Count;
            }
        }

        public Uri Link
        {
            get
            {
                return _Channel.Link;
            }
        }

        public Uri URL
        {
            get
            {
                return _Channel.URL;
            }
        }

        public ObservableCollection<ArticleView> Articles
        {
            get;
        }

        public ArticleView SelectedArticle
        {
            get
            {
                return _SelectedArticle;
            }
            set
            {
                _SelectedArticle = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Commands
        public ICommand ChannelLinkCommand
        {
            get;
        }
        #endregion


        public CategoryView(Channel channel)
        {
            _Channel = channel;

            Articles = new ObservableCollection<ArticleView>(from article in _Channel.Articles 
                                                             select new ArticleView(article));

            ChannelLinkCommand = new ActionCommand(OpenChannelLink, CanOpenChannelLink);
        }


        private void OpenChannelLink()
        {
            try
            {
                Process.Start(_Channel.Link.AbsoluteUri);
            }
            catch(Win32Exception e)
            {
                Trace.Fail($"Cannot open article in the browser due to Win32 Error - {e.Message}");
            }
        }

        private bool CanOpenChannelLink()
        {
            return _Channel.Link != null && !string.IsNullOrEmpty(_Channel.Link.AbsoluteUri);
        }
    }
}
