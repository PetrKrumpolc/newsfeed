﻿using System;
using System.IO;


namespace News.Plugins
{
    public delegate void NewPluginHandler(string path);
    public delegate void DeletePluginHandler(string path);
    public delegate void RenamedPluginHandler(string oldPath, string newPath);

    public class PluginWatcher : IDisposable
    {
        private FileSystemWatcher _Watcher;


        public NewPluginHandler     NewPlugin;
        public DeletePluginHandler  DeletePlugin;
        public RenamedPluginHandler RenamedPlugin;

        public PluginWatcher()
        {
            _Watcher = new FileSystemWatcher(AppDomain.CurrentDomain.BaseDirectory + "Plugins");

            _Watcher.Filter = "*.dll";

            _Watcher.Created += Created;
            _Watcher.Deleted += Deleted;
            _Watcher.Renamed += Renamed;

            _Watcher.EnableRaisingEvents = true;
        }

        private void Renamed(object sender, RenamedEventArgs e)
        {
            RenamedPlugin?.Invoke(e.OldFullPath, e.FullPath);
        }

        private void Deleted(object sender, FileSystemEventArgs e)
        {
            DeletePlugin?.Invoke(e.FullPath);
        }

        private void Created(object sender, FileSystemEventArgs e)
        {
            NewPlugin?.Invoke( e.FullPath);
        }


        public void Dispose()
        {
            _Watcher.Dispose();
        }
    }
}
