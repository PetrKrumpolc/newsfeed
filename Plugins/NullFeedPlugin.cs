﻿using System;
using System.Collections.Generic;

using IPlugin;
using IPlugin.Feeds;

namespace News.Plugins
{
    public class NullFeedPlugin : IFeedPlugin
    {
        public string Company
        {
            get { return string.Empty; }
        }

        public Uri LogoSource
        {
            get { return new Uri(string.Empty); }
        }

        public IEnumerable<Channel> Channels
        {
            get { return new Channel[0]; }
        }

        public bool IsInitialized
        {
            get
            {
                return true;
            }
        }

        public void Destroy()
        {
        }

        public void Initialize()
        {
        }

        public void Update()
        {
        }
    }
}
