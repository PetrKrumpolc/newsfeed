﻿using System;
using System.Collections.Generic;

using IPlugin.Feeds;

namespace IPlugin
{
    public interface IFeedPlugin
    {
        string Company
        {
            get;
        }

        Uri LogoSource
        {
            get;
        }

        bool IsInitialized
        {
            get;
        }

        IEnumerable<Channel> Channels
        {
            get;
        }

        void Initialize();

        void Update();

        void Destroy();
    }
}
