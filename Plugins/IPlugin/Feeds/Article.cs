﻿using System;

namespace IPlugin.Feeds
{
    public class Article
    {
        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Uri Link
        {
            get;
            set;
        }

        public DateTime Published
        {
            get;
            set;
        }

        public Uri ThumbnailSource
        {
            get;
            set;
        }
    }
}
