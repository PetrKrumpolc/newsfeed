﻿using System;
using System.Collections.Generic;

namespace IPlugin.Feeds
{
    public class Channel
    {
        #region Properties
        public Uri URL
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Uri Link
        {
            get;
            set;
        }

        public Uri ImageSource
        {
            get;
            set;
        }

        public IList<Article> Articles
        {
            get;
        }
        #endregion

        public Channel()
        {
            Articles = new List<Article>();
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
