﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Syndication;
using System.Xml;

namespace IPlugin.Feeds.IO
{
    public class FeedReader
    {
        private readonly string _Url;
        private static readonly string[] IMAGES = { ".jpg", ".jpeg", ".png" };


        public FeedReader(string url)
        {
            _Url = url;
        }


        public Channel Deserialize()
        {
            Channel channel = new Channel();
            channel.URL     = new Uri(_Url);

            using (XmlReader reader = XmlReader.Create(_Url, new XmlReaderSettings() { IgnoreWhitespace = true }))
            {
                SyndicationFeed feed = SyndicationFeed.Load(reader);

                Deserialize(channel, feed);

                foreach(SyndicationItem item in feed.Items)
                    if(item.Title != null)
                        channel.Articles.Add(Deserialize(item));
            }
            
            return channel;
        }


        private void Deserialize(Channel channel, SyndicationFeed feed)
        {
            channel.Title       = Deserialize(feed.Title);
            channel.Description = Deserialize(feed.Description);
            channel.Link        = feed.Links.Count > 0 ? feed.Links[0].Uri : null;
            channel.ImageSource = feed.ImageUrl != null ? feed.ImageUrl : new Uri(AppDomain.CurrentDomain.BaseDirectory + "noImage.png");
        }

        private Article Deserialize(SyndicationItem item)
        {
            Article article = new Article();

            article.Title           = Deserialize(item.Title);
            article.Description     = Deserialize(item.Summary);
            article.Link            = item.Links.Count > 0 ? item.Links[0].Uri : null;
            article.ThumbnailSource = Find(item.Links);
            article.Published       = item.PublishDate.DateTime;

            return article;
        }

        private string Deserialize(TextSyndicationContent content)
        {
            return content != null ? content.Text.Trim() : string.Empty;
        }


        private Uri Find(IEnumerable<SyndicationLink> links)
        {
            foreach (SyndicationLink link in links)
                if (IsImage(link))
                    return link.Uri;

            return null;
        }

        private bool IsImage(SyndicationLink link)
        {
            string url = link.Uri.AbsolutePath.ToLower();

            if (!string.IsNullOrEmpty(link.MediaType) && link.MediaType.Contains("image"))
                return true;

            foreach (string type in IMAGES)
                if (url.Contains(type))
                    return true;

            return false;
        }
    }
}
