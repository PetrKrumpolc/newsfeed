﻿using System;
using System.Collections.Generic;

using CzechTV.IO;

using IPlugin;
using IPlugin.Feeds;

namespace CzechTV
{
    public class CzechTvPlugin : IFeedPlugin
    {
        #region Properties
        public IEnumerable<Channel> Channels
        {
            get;
            private set;
        }

        public string Company
        {
            get;
        }

        public Uri LogoSource
        {
            get;
        }

        public bool IsInitialized
        {
            get;
            private set;
        }
        #endregion

        public CzechTvPlugin()
        {
            Channels   = new Channel[0];
            Company    = "Česká Televize";
            LogoSource = new Uri("https://img.ceskatelevize.cz/header/gfx/responsive/logo-ceskatelevize.png");
        }

        public void Destroy()
        {
            AppDomain.Unload(AppDomain.CurrentDomain);
        }

        public void Initialize()
        {
            Update();
            IsInitialized = true;
        }

        public void Update()
        {
            Channels = new CzechTvLoader().LoadAll();
        }
    }
}
