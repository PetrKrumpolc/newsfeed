﻿using System.Collections.Generic;

using IPlugin.Feeds;
using IPlugin.Feeds.IO;

namespace CzechTV.IO
{
    public class CzechTvLoader
    {
        private static readonly string[] FEEDS = { "https://ct24.ceskatelevize.cz/rss/hlavni-zpravy",
                                                   "https://ct24.ceskatelevize.cz/rss/morava",
                                                   "https://ct24.ceskatelevize.cz/rss/slezsko"};

        public IEnumerable<Channel> LoadAll()
        {
            List<Channel> channels = new List<Channel>();

            foreach (string url in FEEDS)
                channels.Add(new FeedReader(url).Deserialize());

            return channels;
        }
    }
}
