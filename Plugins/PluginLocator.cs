﻿using System;
using System.Collections.Generic;
using System.IO;

namespace News.Plugins
{
    public class PluginLocator
    {
        public IEnumerable<string> FindPlugins()
        {
            return Directory.GetFiles($"{AppDomain.CurrentDomain.BaseDirectory}Plugins", "*.dll");
        }
    }
}
