﻿using System;
using System.Collections.Generic;

using CNN.IO;

using IPlugin;
using IPlugin.Feeds;

namespace CNN
{
    public class CNNPlugin : IFeedPlugin
    {
        #region Properties
        public string Company
        {
            get;
        }

        public Uri LogoSource
        {
            get;
        }

        public bool IsInitialized
        {
            get;
            private set;
        }

        public IEnumerable<Channel> Channels
        {
            get;
            private set;
        }
        #endregion


        public CNNPlugin()
        {
            Company       = "CNN";
            LogoSource    = new Uri("http://cdn.cnn.com/cnn/.e1mo/img/4.0/logos/logo_cnn_badge_2up.png");
            IsInitialized = false;
        }


        public void Destroy()
        {
            AppDomain.Unload(AppDomain.CurrentDomain);
        }

        public void Initialize()
        {
            Update();
            IsInitialized = true;
        }

        public void Update()
        {
            Channels = new List<Channel>(new CnnLoader().LoadAll());
        }
    }
}
