﻿using System.Collections.Generic;

using IPlugin.Feeds;
using IPlugin.Feeds.IO;


namespace CNN.IO
{
    public class CnnLoader
    {
        private static readonly string[] FEEDS = { "http://rss.cnn.com/rss/edition_europe.rss",
                                                   "http://rss.cnn.com/rss/edition.rss",
                                                   "http://rss.cnn.com/rss/edition_world.rss",
                                                   "http://rss.cnn.com/rss/edition_americas.rss"};


        public IEnumerable<Channel> LoadAll()
        {
            List<Channel> channels = new List<Channel>();

            foreach (string url in FEEDS)
                channels.Add(new FeedReader(url).Deserialize());

            return channels;
        }
    }
}
