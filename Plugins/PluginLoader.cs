﻿using System;
using System.IO;
using System.Reflection;

using IPlugin;

namespace News.Plugins
{
    public class PluginLoader
    {
        public IFeedPlugin Load(string dllPath)
        {
            AppDomain pluginDomain = AppDomain.CreateDomain(Path.GetFileName(dllPath));
            Assembly assembly      = pluginDomain.Load(Path.GetFileNameWithoutExtension(dllPath));
            IFeedPlugin plugin     = Activate(assembly);

            return plugin;
        }


        private IFeedPlugin Activate(Assembly assembly)
        {
            return Activator.CreateInstance(FindFeedPlugin(assembly)) as IFeedPlugin;
        }

        private Type FindFeedPlugin(Assembly assembly)
        {
            foreach (Type type in assembly.GetExportedTypes())
                if (typeof(IFeedPlugin).IsAssignableFrom(type) && !type.IsAbstract && !type.IsInterface)
                    return type;

            throw new InvalidPluginException(assembly.FullName);
        }
    }
}
