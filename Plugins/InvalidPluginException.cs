﻿using System;

namespace News.Plugins
{
    public class InvalidPluginException : Exception
    {
        public InvalidPluginException(string pluginPath) 
            : base(pluginPath)
        {
        }
    }
}
