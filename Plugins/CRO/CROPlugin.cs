﻿using System;
using System.Collections.Generic;

using CRO.IO;

using IPlugin;
using IPlugin.Feeds;


namespace CRO
{
    public class CROPlugin : IFeedPlugin
    {
        #region Properties
        public string Company
        {
            get;
        }

        public Uri LogoSource
        {
            get;
        }

        public bool IsInitialized
        {
            get;
            private set;
        }

        public IEnumerable<Channel> Channels
        {
            get;
            private set;
        }
        #endregion


        public CROPlugin()
        {
            Company    = "Český Rozhlas";
            LogoSource = new Uri(AppDomain.CurrentDomain.BaseDirectory + "Plugins/CRo-Cesky_rozhlas-V-WHITE.png");
            Channels   = new Channel[0];
        }


        public void Destroy()
        {
            AppDomain.Unload(AppDomain.CurrentDomain);
        }

        public void Initialize()
        {
            Update();
            IsInitialized = true;
        }

        public void Update()
        {
            Channels = new List<Channel>(new CROLoader().LoadAll());
        }
    }
}
