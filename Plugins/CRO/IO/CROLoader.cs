﻿using System.Collections.Generic;

using IPlugin.Feeds;
using IPlugin.Feeds.IO;

namespace CRO.IO
{
    public class CROLoader
    {
        private static readonly string[] FEEDS = { "https://www.irozhlas.cz/rss/irozhlas",
                                                   "https://www.irozhlas.cz/rss/irozhlas/section/zpravy-domov"};

        public IEnumerable<Channel> LoadAll()
        {
            List<Channel> channels = new List<Channel>();

            foreach (string url in FEEDS)
                channels.Add(new FeedReader(url).Deserialize());

            return channels;
        }
    }
}
