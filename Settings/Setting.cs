﻿namespace News.Settings
{
    public class Setting
    {
        public string Company
        {
            get;
            set;
        }

        public string ChannelUrl
        {
            get;
            set;
        }


        public Setting()
        {
            Company    = string.Empty;
            ChannelUrl = string.Empty;
        }
    }
}
