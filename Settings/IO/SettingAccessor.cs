﻿using System.IO;

using System.Xml.Serialization;

namespace News.Settings.IO
{
    public class SettingAccessor
    {
        public Setting Load(Stream stream)
        {
            using (stream)
                return new XmlSerializer(typeof(Setting)).Deserialize(stream) as Setting;
        }

        public void Save(Stream stream, Setting setting)
        {
            using (stream)
                new XmlSerializer(typeof(Setting)).Serialize(stream, setting);
        }
    }
}
